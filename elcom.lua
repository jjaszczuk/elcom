#!/usr/bin/env lua5.4

-- Ustaw locale:
--os.setlocale("")

-- Importuj potrzebne biblioteki:
local argparse = require("argparse")
local http_server = require("http.server")
local http_headers = require("http.headers")
local http_websocket = require("http.websocket")
local sqlite3 = require("lsqlite3")
local json = require("dkjson")
--local inspect = require("inspect")

-- Funkcja wypisuje na standardowe wyjście błędów komunikat z datą:
local function print_mes(mes)
    io.write(os.date() .. ": " .. mes)
end

-- Odczytaj argumenty z linii poleceń:
local parser = argparse({
    description = "Serwer WebSocket elcom.",
    epilog = "Więcej informacji: https://gitlab.com/jjaszczuk/elcom."
})
parser:option("-d --database_file", "Plik z bazą danych.", "/var/lib/elcom/base.db")
parser:flag("--debug", "Wyświetlaj dodatkowe informacje debugowe na HTTP.")
local args = parser:parse()

-- Otwórz bazę danych:
local db, err_code, err_mes = sqlite3.open(args.database_file)
if not db
then
    print_mes(tostring(err_code) .. " " .. err_mes .. ": " .. args.database_file .. "\n")
    os.exit(1)
end

-- Utwórz zapytanie do bazy danych:
local stmt_find_in_db = db:prepare("SELECT * FROM ELEMENTY3 WHERE OPIS LIKE (?)")
-- Funkcja zwraca tabelę ze znalezionymi wynikami:
local function find_in_db(search_text)
    -- Sformatuj zapytanie:
    search_text = "%" .. search_text .. "%"
    stmt_find_in_db:bind_values(search_text)

    -- Tabela do zapisania odpowiedzi:
    local answer = {head = {}, body = {}, search_status = ""}

    -- Ustaw nagłówki tabeli:
    answer.head = stmt_find_in_db:get_names()

    -- Wypełnij ciało tabeli:
    for row in stmt_find_in_db:rows()
	do
        table.insert(answer.body, row)
    end

    -- Uzupełnij status:
    answer.search_status = "Liczba znalezionych rekordów: " .. tostring(#answer.body) .. "."

    -- Resetuj zapytanie:
    stmt_find_in_db:reset()

    return answer
end

-- Funkcja obsługi serwera w której może pojawić się błąd:
local function server_callback_unsafe(server, stream)
    -- Przeczytaj nagłówki zapytania:
    local request_headers = assert(stream:get_headers())
    local request_method = request_headers:get(":method")
    local request_path = request_headers:get(":path")
    local _, request_ip, request_port = stream:peername()
    request_port = tostring(request_port)

    -- Wypisz informacje o połączeniu:
    print_mes(
        request_ip .. ":" .. request_port .. " " ..
        request_method .. " " ..
        request_path .. " " ..
        request_headers:get("user-agent") .. "\n"
    )

    -- Sprawdź do jakiej ścieżki się połączono:
    if request_path == "/websocket"
    then
        -- Kiedy połączono z WebScoket:
        local ws = http_websocket.new_from_stream(stream, request_headers)
        ws:accept()
        for search_text in ws:each()
        do
            local response = json.encode(find_in_db(search_text))
            print_mes(
                "Websocket: " ..
                request_ip .. ":" .. request_port .. " " ..
                --string.gsub(search_text, "%W", "?") .. "\n"
                search_text .. "\n"
            )
            ws:send(response)
        end
        ws:close()
        print_mes(
        "Websocket close: " ..
        request_ip .. ":" .. request_port .. "\n"
        )
        return
    else
        -- Kiedy połączono z czymś innym:
        -- Twórz odpowiedź http:
        local response_headers = http_headers.new()
        response_headers:append(":status", "404")
        response_headers:append("content-type", "text/plain")

        -- Wyślij nagłówki, jeżeli pytano tylko o nie to zakończ połączenie:
        assert(stream:write_headers(response_headers, request_method == "HEAD"))
        if request_method ~= "HEAD" then
            -- Wyślij ciało i zakończ połączenie:
            assert(stream:write_chunk("404 Not Found\n", true))
        end
    end
end

-- Funkcja callback serwera dla nowych połączeń, obsługuje błędy serwera:
local function server_callback(server, stream)
    local status, error_obj = pcall(server_callback_unsafe, server, stream)

    -- Sprawdź czy nie pojawiły się jakieś błędy:
    if not status
    then
        -- Wypisz komunikat błędu:
        print_mes(
        "500 Internal Server Error:" .. " " ..
        tostring(error_obj) .. "\n"
    )

        -- Odpowiedź do serwera z kodem 500:
        local response_headers = http_headers.new()
        response_headers:append(":status", "500")
        response_headers:append("content-type", "text/plain")

        assert(stream:write_headers(response_headers, false))
        if args.debug
        then
            assert(stream:write_chunk("500 Internal Server Error\n" .. tostring(error_obj) .. "\n", true))
        else
            assert(stream:write_chunk("500 Internal Server Error\n", true))
        end
    end
end

local server_params = {
    host = "localhost",
    port = 5678,
    onstream = server_callback,
    onerror = function(myserver, context, op, err, errno)
		local msg = op .. " on " .. tostring(context) .. " failed"
		if err then
			msg = msg .. ": " .. tostring(err)
		end
		print_mes(msg .. "\n")
	end
}

-- Tworzenie obiektu serwera:
local server = assert(http_server.listen(server_params))

-- Ustaw nasłuchiwanie serwera:
assert(server:listen())

-- Poinformuj o starcie serwera:
io.stderr:write("Start serwera WebScoket elcom!\n")

-- Główna pętla serwera:
assert(server:loop())